# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Infoblox_netmri System. The API that was used to build the adapter for Infoblox_netmri is usually available in the report directory of this adapter. The adapter utilizes the Infoblox_netmri API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Infoblox NetMRI adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Infoblox NetMRI. With this adapter you have the ability to perform operations with Infoblox NetMRI on items such as:

- Device Groups
- Devices
- Interfaces

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
