# Infoblox NetMRI

Vendor: Infoblox
Homepage: https://www.infoblox.com/

Product: NetMRI
Product Page: https://www.infoblox.com/products/netmri/

## Introduction
We classify Infoblox NetMRI into the Service Assurance domain as Infoblox NetMRI ensures network compliance, security, and efficiency through comprehensive discovery, analysis, and automation capabilities.

## Why Integrate
The Infoblox NetMRI adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Infoblox NetMRI. With this adapter you have the ability to perform operations with Infoblox NetMRI on items such as:

- Device Groups
- Devices
- Interfaces

## Additional Product Documentation
The [API documents for Infoblox NetMRI](https://docs.infoblox.com/space/APIDeveloperGuide/42861753/NetMRI+API+Protocol)
