
## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-infoblox_netmri!1

---

## 0.1.1 [10-28-2021]

- Initial Commit

See commit ba5f39d

---
