
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:17PM

See merge request itentialopensource/adapters/adapter-infoblox_netmri!11

---

## 0.4.3 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-infoblox_netmri!9

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:27PM

See merge request itentialopensource/adapters/adapter-infoblox_netmri!8

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:40PM

See merge request itentialopensource/adapters/adapter-infoblox_netmri!7

---

## 0.4.0 [05-20-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-infoblox_netmri!6

---

## 0.3.3 [03-27-2024]

* Changes made at 2024.03.27_13:13PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-infoblox_netmri!5

---

## 0.3.2 [03-12-2024]

* Changes made at 2024.03.12_10:58AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-infoblox_netmri!4

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:33AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-infoblox_netmri!3

---

## 0.3.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-infoblox_netmri!2

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-infoblox_netmri!1

---

## 0.1.1 [10-28-2021]

- Initial Commit

See commit ba5f39d

---
